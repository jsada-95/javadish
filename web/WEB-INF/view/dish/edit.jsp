<%@page import="model.Dish"%>
<%@include file="/WEB-INF/view/header.jsp" %>
<jsp:useBean id="dish" class="model.Dish" scope="request"/>  

<div id="content">
    <% if(dish.getId() == 0){%>
    <h1> Registro no encontrado </h1>
    <% } else { %>
    <h1>Edici�n de Platos </h1>
    <form action="<%= request.getContextPath()%>/dish/update" 
          method="post">
        <input name="id" type="hidden" 
               value="<%= dish.getId()%>"><br>
        
        <label>User</label><input name="user" type="text" value="<%= dish.getUser()%>"><br>
        <label>Tipo</label><input name="type" type="text" value="<%= dish.getType()%>"><br>
        <label>Nombre</label><input name="name" type="text" value="<%= dish.getName()%>"><br>
        <label>Descripcion</label><input name="description" type="text" value="<%= dish.getDescription()%>"><br>
        
        <input value="Guardar" type="submit"><br>
    </form>    
    <% } %>
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
