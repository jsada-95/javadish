

<%@page import="model.Dish"%>
<jsp:include page="/WEB-INF/view/header.jsp"/>   
<jsp:useBean id="dish" class="Dish" scope="request"/>  

<div id="content">        
    <h1>Localizar profesor</h1>
    <ul>
        <li><%= dish.getId() %></li>
        
        <li><%= dish.getUser() %></li>
        <li><%= dish.getType() %></li>
        <li><%= dish.getName() %></li>
        <li><%= dish.getDescription() %></li>
    </ul>
</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>