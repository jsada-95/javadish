
<%@page import="model.Dish"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="dishes" class="java.util.ArrayList" scope="request"/>  


<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Lista de platos </h1>
    <p>
        <a href="<%= request.getContextPath()%>/dish/create">
                                            Nuevo plato</a>
        <a href="<%= request.getContextPath()%>/dish/seek">
                                            Buscar plato</a>
    </p>
    <table>
        <tr>
            <th>Id</th>
            <th>User</th>
            <th>Type</th>
            <th>Nombre</th>
            <th>Descripción</th>
        </tr>
        <%        Iterator<model.Dish> iterator = dishes.iterator();
            while (iterator.hasNext()) {
                Dish dish = iterator.next();%>
        <tr>
            <td><%= dish.getId()%></td>
            <td><%= dish.getUser()%></td>
            <td><%= dish.getType()%></td>
            <td><%= dish.getName()%></td>
            <td><%= dish.getDescription()%></td>
        
            <td> 
                <a href="<%= request.getContextPath() + "/dish/delete/" + dish.getId()%>"> Borrar</a>
                <a href="<%= request.getContextPath() + "/dish/edit/"  + dish.getId()%>"> Editar</a>
                <a href="<%= request.getContextPath() + "/dish/guardar/" + dish.getUser()%>"> Guardar</a>
            </td>
        </tr>
        <%
            }
        %>          
    </table>
    <a href="<%= request.getContextPath() + "/dish/index/1" %>"> 1</a>
    <a href="<%= request.getContextPath() + "/dish/index/2" %>"> 2</a>
    <a href="<%= request.getContextPath() + "/dish/index/3" %>"> 3</a>
    <br>
    
    


   
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
