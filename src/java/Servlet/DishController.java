/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import static java.lang.Long.parseLong;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import model.Dish;
import persistence.DishDAO;

/**
 *
 * @author usuario
 */
public class DishController extends BaseController {
    
    private static final int ELEMENTS_PER_PAGE = 2;
    private static final Logger LOG = Logger.getLogger(DishController.class.getName());
    private DishDAO dishDAO;

    public void index(String page) {
        
        int pageNum = 1;
        //objeto persistencia
        dishDAO = new DishDAO();
        ArrayList<Dish> dishes = null;
        
        pageNum = Integer.parseInt(page);
         
        //leer datos de la persistencia
        synchronized (dishDAO) {
            dishes = dishDAO.getAll(pageNum, ELEMENTS_PER_PAGE);
        } //para que no haya 2 conexiones a la vez
        request.setAttribute("dishes", dishes);
        String name = "index";
        LOG.info("En Index de DishController");
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/dish/index.jsp");
    }

    public void create() {
        dispatch("/WEB-INF/view/dish/create.jsp");
    }
    
    public void insert() throws IOException {

        //objeto persistencia
        dishDAO = new DishDAO();
        LOG.info("crear DAO");
        //crear objeto del formulario
        Dish dish = loadFromRequest();
        
        synchronized (dishDAO) {
            try {
                dishDAO.insert(dish);
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
                request.setAttribute("ex", ex);
                request.setAttribute("msg", "Error de base de datos ");
                dispatch("/WEB-INF/view/error/index.jsp");
            }
        }
        redirect(contextPath + "/dish/index/1");
    }
    
    public void edit(String idString) throws SQLException {
        LOG.info(""+idString);
        long id = toId(idString);
        DishDAO  dishDAO = new DishDAO();
        Dish dish = dishDAO.get(id);
        LOG.info("probando");
        request.setAttribute("dish", dish);
        dispatch("/WEB-INF/view/dish/edit.jsp");

    }

    public void update() throws IOException, SQLException {
        DishDAO  dishDAO = new DishDAO();
        LOG.info("en update");
        Dish dish = loadFromRequest(); 
        dishDAO.update(dish);
        response.sendRedirect(contextPath + "/dish/index/1");
        return;
    }
    
    public void delete(String idString) throws IOException, SQLException 
    {
        LOG.info("BORRANDO " + idString);
        long id = toId(idString);
        
        DishDAO  dishDAO = new DishDAO();
        dishDAO.delete(id);
        redirect(contextPath + "/dish/index/1");
        
    }
 
    
    private Dish loadFromRequest()
    {
        Dish dish = new Dish();
        LOG.info("Crear modelo");
        


        dish.setId(toId(request.getParameter("id")));
        dish.setUser(request.getParameter("user"));
        dish.setType(request.getParameter("type"));
        dish.setName(request.getParameter("name"));
        dish.setDescription(request.getParameter("description"));
        
        LOG.info("Datos cargados");
        return dish;
    }
    
     
    public void guardar(String userName) throws SQLException {

        HttpSession session = request.getSession(true);
        session.setAttribute("name", userName);

        redirect(contextPath + "/dish/index/1");

    }
    public void seek() throws SQLException {
        dispatch("/WEB-INF/view/dish/seek.jsp");
    }
    
     public void found() throws SQLException {
        dishDAO = new DishDAO();
        Dish dish = new Dish();
        long id;
        
        id = parseLong(request.getParameter("numero"));
        synchronized (dishDAO) {
            dish = dishDAO.get(id);
        } 
        request.setAttribute("dish", dish);
        dispatch("/WEB-INF/view/dish/found.jsp");

    }
}
