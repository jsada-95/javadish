/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import model.Dish;

/**
 *
 * @author usuario
 */
public class DishDAO extends BaseDAO{
    public DishDAO() {
//        Class.forName("com.mysql.jdbc.Driver");
//        this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    }
    public ArrayList<Dish> getAll(int page, int elementsPerPage) {
        PreparedStatement stmt = null;
        ArrayList<Dish> dishes = null;
        int offset = (page - 1) * elementsPerPage;
        
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from dishes LIMIT " + elementsPerPage + " OFFSET " + offset);
            ResultSet rs = stmt.executeQuery();
            dishes = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Dish dish = new Dish();
                dish.setId(rs.getLong("id"));
                dish.setUser(rs.getString("user"));
                dish.setType(rs.getString("type"));
                dish.setName(rs.getString("name"));
                dish.setDescription(rs.getString("description"));

                dishes.add(dish);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return dishes;
    }
   public Dish get(long id) throws SQLException {
        LOG.info("get(id)");
        Dish dish = new Dish();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM dishes"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        LOG.info("consulta hecha");
        if (rs.next()) {
            LOG.info("Datos ...");
            dish.setId(rs.getLong("id"));
            dish.setUser(rs.getString("user"));
            dish.setType(rs.getString("type"));
            dish.setName(rs.getString("name"));
            dish.setDescription(rs.getString("description"));
            LOG.info("Datos cargados EN DAO GET");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        this.disconnect();
        return dish;
    }
   
    public void insert(Dish dish) throws SQLException {
        PreparedStatement stmt = null;
        LOG.info("Crear DAO");
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO dishes(user, type, name, description)"
                + " VALUES(?, ?, ?, ?)"
        );
        //stmt.setLong(1, 4); // teacher.getId()
        
        //LOG.info(""+teacher.getId());
        stmt.setString(1, dish.getUser());
        stmt.setString(2, dish.getType());
        stmt.setString(3, dish.getName());
        stmt.setString(4, dish.getDescription());
        

        stmt.execute();
        this.disconnect();
    }
    
     public void update(Dish dish) throws SQLException {
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "UPDATE dishes SET "
                        + "user = ?, "
                        + "type = ?, "
                        + "name = ?, "
                        + "description = ? "
                        + " WHERE id = ? "
        );
        stmt.setString(1, dish.getUser());
        stmt.setString(2, dish.getType());
        stmt.setString(3, dish.getName());
        stmt.setString(4, dish.getDescription());
        stmt.setLong(5, dish.getId());
        LOG.info(""+stmt);
        
        stmt.execute();
        LOG.info("consulta hecha");
        this.disconnect();
        return;
    }
     public void delete(long id) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "DELETE FROM dishes"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        stmt.execute();
        this.disconnect();
    }
    
}
